# Tic-Tac-Toe

This project is an experimental dive into Layer Based Programming

# Layer based programming

 - All classes should abide the laws of the layer it is defined in

## Layer 1 - Service Layer
 
 - Services should be coupled to 1) services and 5) entities
 - Services should never raise and exception
 - Services should return a report of the execution, in order
 - Services should be the sum of its operations
 
## Layer 2 - Action Layer

 - Actions should be atomic
 - Actions should be coupled to 3) operations, and/or 4) multiplexers, and 5) primitives
 - Actions should return the mutations that happened
 - Actions should act as a single point of failure
 
## Layer 3 - Multiplexing Layer

 - Multiplexers should be coupled to 4) operations and/or 5) entities 
 - Multiplexers should act on batches data
 - Multiplexers should 1) aggregate the failures and successes (optimistic) or 2) stop at first error (pessimistic)
 - Multiplexers should prepare the data to be process (N+1 optimizations)
 - Multiplexers should act on batches of homogenous entities

## Layer 4 - Operation Layer

 - Operations should be coupled to 5) entities
 - Operations should act on a single entity
 - Operations should be composable if they are in inheritance chain (op1 + op2 = op2(op(1)))
 - Operations may raise
 - Operations should handle the logging

## Layer 5 - Entity Layer

  - Entities should be coupled to 5) entities
  - Entities should be data classes
  - Entities should be copyable (shallow)
  - Entities should be clonable (deep)
  - Entities should be equatable
  - Entities should be the sum of its canonical form, therefore a primitive is considered an entity
 
# License

MIT
