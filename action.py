import functools
from dataclasses import dataclass
from enum import Enum
from typing import Optional

from diff import Diff


class ActionStatus(Enum):
    SUCCESS = True
    FAILURE = False


@dataclass
class ActionResult:
    action: object
    status: ActionStatus

    @property
    def success(self):
        return self.status is ActionStatus.SUCCESS


@dataclass
class Failure(ActionResult):
    error: Exception

    def __init__(self, action, error: Exception):
        super().__init__(action, ActionStatus.FAILURE)
        self.error = error


@dataclass
class Success(ActionResult):
    diff: Diff

    def __init__(self, action, diff: Diff):
        super().__init__(action, ActionStatus.SUCCESS)
        self.diff = diff


def action(func):
    @functools.wraps(func)
    def decorated(action, *args, **kwargs):
        try:
            diff = func(action, *args, **kwargs)
            action.target.__apply__(diff)

            return Success(action, diff)
        except Exception as e:
            return Failure(action, e)

    return decorated
