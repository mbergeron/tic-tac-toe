import io
import dataclasses
from typing import Generic, TypeVar, Set, Dict, List


E = TypeVar('E')  # Generic Entity


class DiffConflictError(Exception, Generic[E]):
    def __init__(self, conflicts: Set[str], a: 'Diff[E]', b: 'Diff[E]'):
        self.conflicts = conflicts
        self.a = a
        self.b = b


class Diff(Generic[E]):
    def __init__(self, fields: List[str], a: Dict, b: Dict):
        self.fields = set(fields)
        self.a = a
        self.b = b

    def reverse(self):
        return Diff(self.fields, self.b, self.a)

    def merge(self, other: 'Diff[E]'):
        if not other:
            return self

        conflicts = self.fields & other.fields
        if conflicts:
            raise DiffConflictError(conflicts, self, other)

        return Diff(self.fields | other.fields,
                    {**self.a, **other.a},
                    {**self.b, **other.b})

    def __iter__(self):
        for field in self.fields:
            yield field, (self.a[field], self.b[field])

    def __repr__(self):
        buffer = io.StringIO()
        isset = lambda x: x is not None

        for f in self.fields:
            buffer.write(f"{f} <<<<<<\n")
            if isset(self.a[f]):
                buffer.write(f"{self.a[f]}\n")
            buffer.write("======\n")
            if isset(self.b[f]):
                buffer.write(f"{self.b[f]}\n")
            buffer.write(f"{f} >>>>>>\n")

        return buffer.getvalue()


class Diffable:
    def __diff__(self, other):
        self_dict = dataclasses.asdict(self)
        other_dict = dataclasses.asdict(other)

        diff_keys = [k
                     for k, v in self_dict.items()
                     if other_dict[k] != v]

        a_fields = {k: self_dict[k]
                    for k in diff_keys}

        b_fields = {k: other_dict[k]
                    for k in diff_keys}

        # should probably be weakrefs
        return Diff(diff_keys,
                    a_fields,
                    b_fields)

    def __apply__(self, diff: 'Diff[E]') -> 'Diff[E]':
        for k, mut in diff:
            _, to = mut
            setattr(self, k, to)

        return diff.reverse()
