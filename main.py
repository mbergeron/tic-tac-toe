import curses
import dataclasses
import functools
import re
from copy import deepcopy
from enum import Enum
from typing import TypeVar, Dict, Generic, Iterator, List, Tuple, Optional
from itertools import chain
import clr

from diff import Diffable, Diff
from action import action
from operation import operation
from memento import UndoService


def compose(*functions):
    identity = lambda x: x
    def f_rond_g(f, g):
        return lambda x: f(g(x))

    return functools.reduce(f_rond_g, functions, identity)


class InvalidOperationError(Exception):
    pass


class Symbol(Enum):
    X = clr.red('X')
    O = clr.yellow('O')
    Unset = ' '

    def __str__(self):
        return self.value


@dataclasses.dataclass
class Game(Diffable):
    """
    [0, 1, 2,
     3, 4, 5,
     6, 7, 8]
    """
    board: Tuple[Symbol] = tuple([Symbol.Unset] * 9)
    turn: int = 0
    last_move: Optional[int] = None
    winner: Optional[int] = None
    winner_moves: Tuple[int] = ()


@dataclasses.dataclass
class RenderContext(Diffable):
    render: str = ""
    winner: str = ""


class Action:
    def __init__(self, target: Diffable):
        self._target = target

    @property
    def target(self):
        return self._target


class PlayAction(Action):
    @property
    def game(self):
        return self.target

    @action
    def __call__(self, symbol: Symbol, pos: int) -> Diff:
        # place the correct symbol
        symbol_op = GameSetOperation(pos, symbol).do(self.game)

        # advance the turn
        turn_op = GameNextTurnOperation().do(self.game)
        diff = symbol_op.merge(turn_op)

        return diff


class ResetOperation:
    @classmethod
    @operation
    def reset(cls, target: Diffable):
        return target.__class__()


class ResetAction(Action):
    @action
    def __call__(self) -> Diff:
        diff = ResetOperation.reset(self.target)
        return diff


class CheckWinnerAction(Action):
    @action
    def __call__(self):
        return GameCheckWinnerOperation().do(self.target)


class PlayService:
    def __init__(self, game: Game):
        self._game = game
        self.restart_action = ResetAction(game)
        self.undo_service = UndoService(5)
        self.play_action = PlayAction(self._game)
        self.check_winner_action = CheckWinnerAction(self._game)

    def _player_input(self, symbol: Symbol):
        pos = None
        action = None
        while not any((pos, action)):
            raw = input(f"Turn {self._game.turn + 1} {symbol}, (1-9), [u]ndo:")
            match = re.match(r"([1-9])|(u)", raw)
            if match:
                pos, action = match.groups()

        return pos, action

    def _symbol(self):
        return Symbol.O if self._game.turn % 2 else Symbol.X

    def play(self):
        if self._game.winner_moves:
            self.restart_action()
        else:
            symbol = self._symbol()
            pos, action = self._player_input(symbol)

            if pos is not None:
                play = self.play_action(symbol, int(pos) - 1)
                winner = self.check_winner_action()

                if play.success and winner.success:
                    self.undo_service.memoize(play, winner)
            elif action == "u":
                self.undo_service.undo()

    def __call__(self):
        return self.play()


class GameSetOperation:
    def __init__(self, pos: int, symbol: Symbol):
        self._pos = pos
        self._symbol = symbol

    @operation
    def do(self, game: Game):
        if game.board[self._pos] is not Symbol.Unset:
            raise InvalidOperationError()

        try:
            new_board = list(game.board)
            new_board[self._pos] = self._symbol
            game.board = tuple(new_board)
            game.last_move = self._pos
        except IndexError:
            raise InvalidOperationError()


class GameNextTurnOperation:
    @operation
    def do(self, game: Game):
        # move the counter forward
        game.turn += 1


class GameCheckWinnerOperation:
    @operation
    def do(self, game: Game):
        def check_winner_moves(indices):
            any_unset = compose(any,
                                functools.partial(map, lambda s: s == Symbol.Unset))

            a, b, c = (game.board[i] for i in indices)
            if any_unset((a, b, c)):
                return False

            return a == b == c

        # check diags
        dx = (0, 4, 8)  # diagonal
        ax = (2, 4, 6)  # anti-diagonal

        # check if there is a winner
        rxs = (tuple(range(i*3, i*3+3)) for i in range(0, 3))  # rows
        cxs = (tuple(range(i, 9, 3)) for i in range(0, 3))  # columns

        winner_moves = compose(tuple,
                               chain.from_iterable,
                               functools.partial(filter, check_winner_moves))

        game.winner_moves = winner_moves((dx, ax, *rxs, *cxs))


class RenderAction(Action):
    """
    Renders the board to the screen.

     X | O |
    ---+---+---
     O |   |
    ---+---+---
       | X | O
    """
    @property
    def render_context(self):
        return self.target

    @action
    def __call__(self, game: Game):
        cells = RenderCellOperation(game).do_x(
            render_context,
            range(9)
        )
        winner = RenderWinnerOperation(game).do(self.render_context)

        diff = cells.merge(winner)
        return diff


class RenderCellOperation:
    def __init__(self, game: Game):
        self.game = game

    def symbol(self, pos: int):
        return self.game.board[pos]

    @operation
    def do(self, render_ctx: RenderContext, pos: int):
        col = pos % 3
        symbol = self.symbol(pos)

        if symbol == Symbol.Unset:
            symbol = clr.dim(str(pos + 1))

        elif self.game.last_move == pos:
            symbol = clr.bold(symbol)

        if pos in self.game.winner_moves:
            symbol = clr.underline(symbol)

        render_ctx.render += f" {symbol}"

        # decorate the cell
        if col == 2:
            render_ctx.render += "\n"
            if pos < 8:
                render_ctx.render += clr.dim("---+---+---\n")
        else:
            render_ctx.render += clr.dim(f" |")

    @operation
    def do_x(self, render_ctx, pos_it: Iterator[int]) -> Iterator[Diff]:
        for pos in pos_it:
            diff = self.do(render_ctx, pos)
            render_ctx.__apply__(diff)


class RenderWinnerOperation:
    def __init__(self, game: Game):
        self._game = game

    def _player(self):
        """
        Return the last turn player.
        """
        if (self._game.turn - 1) % 2:
            return Symbol.O
        else:
            return Symbol.X

    @operation
    def do(self, render_ctx: RenderContext):
        if self._game.winner_moves:
            render_ctx.winner = clr.blue.bold(f"Winner is: {self._player()}")


class DisplayService:
    """Manage the on-screen display for every frame."""
    def __init__(self, game: Game, render_ctx: RenderContext):
        self._game = game
        self._render_ctx = render_ctx
        self.reset = ResetAction(render_ctx)
        self.render_action = RenderAction(render_ctx)

    def render(self):
        self.reset()
        self.render_action(self._game)
        print("\n")
        print(self._render_ctx.render)
        print(self._render_ctx.winner)

    def __call__(self,):
        return self.render()


if __name__ == '__main__':
    game = Game()
    render_context = RenderContext()
    display = DisplayService(game, render_context)
    play = PlayService(game)

    while True:
        display()
        play()
