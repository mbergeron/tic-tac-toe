from collections import deque
from diff import Diff, Diffable


STACK_SIZE = 100


def memento(cls):
    class Decorated(cls):
        def __init__(self, stack_size: int = STACK_SIZE):
            super().__init__()
            self._stack = deque([], stack_size)

        def revert(self, step=1) -> Diff:
            """
            Revert n steps of the memento
            """
            diffs = [self._stack.pop()
                     for i in range(0, step)]

            for diff in diffs:
                self.__apply__(diff)

            return diffs

        def __apply__(self, diff: Diff):
            super().__apply__(diff)
            self._stack.append(diff)

    return Decorated


class UndoService:
    def __init__(self, stack_size: int = STACK_SIZE):
        super().__init__()
        self._stack = deque([], stack_size)
        self._cursor = -1  # head

    def memoize(self, *results):
        self._stack.append(results)

    def undo(self, step=1):
        """
        Revert n steps of the memento
        """
        undos = []
        try:
            undos = [self._stack.pop()
                     for i in range(0, step)]
        except IndexError as err:
            pass

        undo_results = (result
                        for undo in undos
                        for result in undo)

        for result in undo_results:
            result.action.target.__apply__(result.diff.reverse())
