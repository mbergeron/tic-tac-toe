import functools
from copy import deepcopy


def operation(func):
    @functools.wraps(func)
    def decorate(self, target, *args):
        mut = deepcopy(target)

        # the operation can now freely mutate the targets
        other = func(self, mut, *args)

        return target.__diff__(other or mut)

    return decorate
